/** @format */

import Header from "../../components/Header/Header";
import BannerHomePage from "../../components/BannerHomePage/BannerHomePage";
import ServicesHomePage from "../../components/ServicesHomePage/ServicesHomePage";
import AboutUsAndProfileHomePage from "../../components/AboutUsAndProfileHomePage/AboutUsAndProfileHomePage";
import ResourcesHomePage from "../../components/ResourcesHomePage/ResourcesHomePage";
import ContactPage from "../../components/ContactPage/ContactPage";
import Footer from "../../components/Footer/Footer";

const HomePage = () => {
  return (
    <>
      <Header />
      <div
        data-bs-spy='scroll'
        data-bs-target='#headerMenuBar'
        data-bs-smooth-scroll='true'
        className='scrollspy-example'
        tabIndex='0'>
        <BannerHomePage />
        <ServicesHomePage />
        <AboutUsAndProfileHomePage />
        <ResourcesHomePage />
        <ContactPage />
      </div>
      <Footer />
    </>
  );
};

export default HomePage;
