/** @format */

import Paragraph from "antd/es/typography/Paragraph";
const listServices = [
  {
    id: 1,
    name: "MOBILE SERVICE",
    image: "/images/service/ser-item-2.png",
    description: (
      <>
        CCS has attained the Mobile Virtual Network Operator License (MVNO) in
        2009, where we provide postpaid mobile plans to our customers. <br />{" "}
        CCS had maintained close working relationship with our existing backend
        providers, in providing attractive and competitive pricing plans
        offering to our customers. <br /> We have built a good base of customers
        who are enjoying good pricing schemes and maintaining a responsive
        customer service team, to assist in resolving their day-to-day mobile
        issues. <br /> We are able to provide an in-depth analysis on your
        existing mobile bills and defined the most economical plans to suit your
        needs.,
      </>
    ),
  },
  {
    id: 2,
    name: "RTONE SERVICE",
    image: "/images/service/ser-item-5.png",
    description: (
      <>
        The R Tone service is a break through effort for our customers to
        affordable roaming services while travelling. <br /> We have been
        spending gruelling effort to design and create an application which is
        able to reside in smart phones, to make and receive calls while
        customers travel overseas. <br /> Though there has been limitation to
        its availability, we are in search of a balance to ensure the solution
        can be consistent and maintain the highest quality for a decent business
        call.
      </>
    ),
  },
  {
    id: 3,
    name: "CCS IINTERNATIONAL CALL FORWARDING (ICF)",
    image: "/images/service/ser-item-6.png",
    description: (
      <>
        The ICF is basically to resolved the issues relating to high cost for
        incoming calls while travelling overseas. <br /> It works with an
        overseas mobile number attained in the foreign destination. In most of
        the countries, incoming calls to their native prepaid mobile number is
        free. <br /> ICF is meant to provide a local number in Singapore that
        will link to your overseas mobile number, so that when the local number
        is called, you will be able to receive the call on your overseas mobile.{" "}
        <br />
        In combining both CCS Callback Service and CCS International Call
        Forward Service, it will bring massive savings to your overall roaming
        bills.
      </>
    ),
  },
  {
    id: 4,
    name: "1505 PREMIUM CONNECT",
    image: "/images/service/ser-item-1.png",
    description: (
      <>
        The 1505 Premium Connect service is an alternative to making Long
        Distance Calls in Singapore. <br />
        We are able to provide uncompromising call quality which is comparable
        to the International Direct Dial (IDD) as offered by Fixed Based
        Operators (FBO). <br />
        We have sinced providing a consistent and robust voice network to our
        faithful customers while enjoying massive savings on their International
        calls.
      </>
    ),
  },
];
const ServicesHomePage = () => {
  return (
    <>
      <section id='services' className='row align-items-center'>
        <div className='col-12'>
          <div className='container'>
            <h1 className='title'>Services</h1>
            <div className='row row-cols-1 row-cols-md-2 align-items-center'>
              {listServices.map((service, index) => {
                return (
                  <>
                    <div className='col' id='ser-items' key={index}>
                      <img src={service.image} alt={service.name} />
                      <h1>{service.name}</h1>
                      <Paragraph
                        id='ser-items-content'
                        key={service.id}
                        ellipsis={{
                          rows: 3,
                          expandable: true,
                          symbol: <i className='fa-solid fa-angle-right'></i>,
                        }}>
                        {service.description}
                      </Paragraph>
                    </div>
                  </>
                );
              })}
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ServicesHomePage;
