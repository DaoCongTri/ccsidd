/** @format */

import { useState } from "react";
import { Form, Input, Select } from "antd";
const { Option } = Select;
const onFinish = (values) => {
  console.log("Success:", values);
};
const onFinishFailed = (errorInfo) => {
  console.log("Failed:", errorInfo);
};
const optionSelectMap = [
  {
    id: "1",
    label: <h1>SINGAPORE</h1>,
    location: "SINGAPORE",
    address: (
      <p>
        63 Kaki Bukit Place #05-01. <br />
        Singapore 416234. <br />
        Phone: +65 67481737. <br />
        Fax: +65 67484812. <br />
        After office hour hotline for <br />
        Mobile suspension: +65 67451505.
        <br />
        Customer Service: <br /> service@ccsidd.com. <br />
        Sales: sales@ccsidd.com.
      </p>
    ),
  },
  {
    id: "2",
    label: (
      <h1>
        VIETNAM <br />
        (Representative Office),
      </h1>
    ),
    location: "VIETNAM",
    address: (
      <p>
        15 Dong Da Street, <br /> District Tan Binh, <br /> Ward 2 Ho Chi Minh
        City.
      </p>
    ),
  },
];
const ContactPage = () => {
  const [defaultMap, setDefaultMap] = useState(
    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.734418912784!2d103.90216307567066!3d1.335567361620145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da17facdccfd85%3A0x892528be69cb328b!2s63%20Kaki%20Bukit%20Pl%2C%20Singapore%20416234!5e0!3m2!1svi!2s!4v1711007545735!5m2!1svi!2s"
  );
  const handleSelectLocation = (location) => {
    switch (location) {
      case "SINGAPORE":
        setDefaultMap(
          "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.734418912784!2d103.90216307567066!3d1.335567361620145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da17facdccfd85%3A0x892528be69cb328b!2s63%20Kaki%20Bukit%20Pl%2C%20Singapore%20416234!5e0!3m2!1svi!2s!4v1711007545735!5m2!1svi!2s"
        );
        break;
      case "VIETNAM":
        setDefaultMap(
          "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1647.751464050565!2d106.66523442984102!3d10.808590728447705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529233a69e3cd%3A0xc8a780fd1823d346!2zMTVBIMSQ4buRbmcgxJBhLCBQaMaw4budbmcgMiwgVMOibiBCw6xuaCwgVGjDoG5oIHBo4buRIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1711015441952!5m2!1svi!2s"
        );
        break;
      default:
        setDefaultMap(
          "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.734418912784!2d103.90216307567066!3d1.335567361620145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da17facdccfd85%3A0x892528be69cb328b!2s63%20Kaki%20Bukit%20Pl%2C%20Singapore%20416234!5e0!3m2!1svi!2s!4v1711007545735!5m2!1svi!2s"
        );
        break;
    }
  };
  return (
    <>
      <section id='contact'>
        <div className='container'>
          <h1 className='title'>CONTACT</h1>
          <div className='row row-cols-1 row-cols-lg-2'>
            <div className='col' id='contact-form'>
              <Form
                name='contactForm'
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete='off'>
                <label htmlFor='username'>Your Name:</label>
                <Form.Item
                  name='username'
                  rules={[
                    {
                      required: true,
                      message: "Please input your username!",
                    },
                  ]}>
                  <Input size='middle' />
                </Form.Item>
                <label htmlFor='username'>Your Email:</label>
                <Form.Item
                  name='email'
                  rules={[
                    {
                      required: true,
                      message: "Please input your email!",
                    },
                    {
                      type: "email",
                      message: "Please input the correct email!",
                    },
                  ]}>
                  <Input size='middle' />
                </Form.Item>
                <label htmlFor='kind'>What is kind of your contact?</label>
                <Form.Item name='kind'>
                  <Select size='large' defaultValue={"customerService"}>
                    <Option value='customerService'>
                      For Customer Service
                    </Option>
                    <Option value='sales'>For Sales</Option>
                    <Option value='billing'>For Billing</Option>
                  </Select>
                </Form.Item>
                <label htmlFor='message'>Your Message:</label>
                <Form.Item
                  name='message'
                  rules={[
                    {
                      required: true,
                      message: "Please enter a message",
                    },
                  ]}>
                  <Input.TextArea maxLength={200} rows={4} size='middle' />
                </Form.Item>
                <Form.Item>
                  <button type='submit' className='me-5 btn btn-primary'>
                    SEND
                  </button>
                  <button type='reset' className='btn btn-primary'>
                    RESET
                  </button>
                </Form.Item>
              </Form>
            </div>
            <div className='col' id='contactMap'>
              <iframe
                src={defaultMap}
                width='100%'
                allowFullScreen
                loading='lazy'
                referrerPolicy='no-referrer-when-downgrade'
              />
              <div
                className='row row-cols-1 row-cols-md-2'
                id='contactLocation'>
                {optionSelectMap.map((item) => {
                  return (
                    <div
                      className='col-auto'
                      id='itemContactLocation'
                      key={item.id}
                      onClick={() => {
                        handleSelectLocation(item.location);
                      }}>
                      {item.label}
                      {item.address}
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ContactPage;
