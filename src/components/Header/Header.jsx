/** @format */

const Header = () => {
  return (
    <>
      <header>
        <div id='header'>
          <nav id='headerMenuBar' className='navbar navbar-expand-lg'>
            <div className='container-fluid'>
              <a id='header-logo' className='navbar-brand' href='#'>
                <img src='/images/logo.png' alt='' />
              </a>
              <button
                className='navbar-toggler'
                type='button'
                data-bs-toggle='collapse'
                data-bs-target='#navbarSupportedContent'
                aria-controls='navbarSupportedContent'
                aria-expanded='false'
                aria-label='Toggle navigation'>
                <i className='fa-solid fa-bars'></i>
              </button>
              <div
                className='collapse navbar-collapse'
                id='navbarSupportedContent'>
                <ul className='nav-pills navbar-nav ms-auto'>
                  <li className='nav-item'>
                    <a className='nav-link active' href='#services'>
                      Services
                    </a>
                  </li>
                  <li className='nav-item'>
                    <a className='nav-link' href='#aboutus'>
                      About
                    </a>
                  </li>
                  <li className='nav-item'>
                    <a className='nav-link' href='#resources'>
                      Resources
                    </a>
                  </li>
                  <li className='nav-item'>
                    <a className='nav-link' href='#contact'>
                      Contact
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </header>
    </>
  );
};

export default Header;
