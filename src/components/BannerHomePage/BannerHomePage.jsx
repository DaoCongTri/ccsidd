/** @format */

const BannerHomePage = () => {
  return (
    <>
      <section id='banner'>
        <div className='container d-flex align-items-center h-100'>
          <div id='banner-content' className='container'>
            <h1>
              Connecting <br /> Communication & <br /> Solutions
            </h1>
            <p>
              Connecting Communication & Solutions Private Limited (CCS) is
              <br />
              service-based operator that specialized in providing overseas call{" "}
              <br />
              service (voice, fax, and inmarsat) to corporate markets
            </p>
            <button>
              <a href='#aboutus'>More About Us</a>
            </button>
          </div>
          <div className='w-25 d-none d-md-block'></div>
        </div>
      </section>
    </>
  );
};

export default BannerHomePage;
