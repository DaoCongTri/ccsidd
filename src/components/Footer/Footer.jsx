/** @format */

const Footer = () => {
  return (
    <>
      <footer className='text-center text-lg-start text-white'>
        <section className='d-flex justify-content-between p-4'>
          <div className='me-5'>
            <span>Get connected with us on social networks:</span>
          </div>
          <div>
            <a href className='text-white me-4'>
              <i className='fab fa-facebook-f' />
            </a>
            <a href className='text-white me-4'>
              <i className='fab fa-twitter' />
            </a>
            <a href className='text-white me-4'>
              <i className='fab fa-google' />
            </a>
            <a href className='text-white me-4'>
              <i className='fab fa-instagram' />
            </a>
            <a href className='text-white me-4'>
              <i className='fab fa-linkedin' />
            </a>
            <a href className='text-white me-4'>
              <i className='fab fa-github' />
            </a>
          </div>
        </section>
        <section>
          <div className='container text-center text-md-start mt-5'>
            <div className='row mt-3'>
              <div className='col-md-3 col-lg-4 col-xl-3 mx-auto mb-4'>
                <h6 className='text-uppercase fw-bold'>
                  Connecting Communications & Solutions
                </h6>
                <hr
                  className='mb-4 mt-0 d-inline-block mx-auto'
                  style={{ width: 60, backgroundColor: "#7c4dff", height: 2 }}
                />
                <p>
                  Connecting Communication & Solutions Private Limited (CCS) is
                  service-based operator that specialized in providing overseas
                  call service (voice, fax, and inmarsat) to corporate markets
                </p>
              </div>
              <div className='col-md-2 col-lg-2 col-xl-2 mx-auto mb-4'>
                <h6 className='text-uppercase fw-bold'>Services</h6>
                <hr
                  className='mb-4 mt-0 d-inline-block mx-auto'
                  style={{ width: 60, backgroundColor: "#7c4dff", height: 2 }}
                />
                <p>
                  <a href='#' className='text-white'>
                    MOBILE SERVICE
                  </a>
                </p>
                <p>
                  <a href='#' className='text-white'>
                    RTONE SERVICE
                  </a>
                </p>
                <p>
                  <a href='#' className='text-white'>
                    ICF Service
                  </a>
                </p>
                <p>
                  <a href='#' className='text-white'>
                    IDD Service
                  </a>
                </p>
              </div>
              <div className='col-md-3 col-lg-2 col-xl-2 mx-auto mb-4'>
                <h6 className='text-uppercase fw-bold'>Useful links</h6>
                <hr
                  className='mb-4 mt-0 d-inline-block mx-auto'
                  style={{ width: 60, backgroundColor: "#7c4dff", height: 2 }}
                />
                <p>
                  <a href='#' className='text-white'>
                    Home
                  </a>
                </p>
                <p>
                  <a href='#services' className='text-white'>
                    Services
                  </a>
                </p>
                <p>
                  <a href='#aboutus' className='text-white'>
                    About
                  </a>
                </p>
                <p>
                  <a href='#resources' className='text-white'>
                    Resources
                  </a>
                </p>
              </div>
              <div className='col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4'>
                <h6 className='text-uppercase fw-bold'>Contact</h6>
                <hr
                  className='mb-4 mt-0 d-inline-block mx-auto'
                  style={{ width: 60, backgroundColor: "#7c4dff", height: 2 }}
                />
                <p>
                  <i className='fas fa-home mr-3' />
                  Singapore 63 Kaki Bukit Place #05-01
                </p>
                <p>
                  <i className='fas fa-envelope mr-3' /> service@ccsidd.com.
                </p>
                <p>
                  <i className='fas fa-phone mr-3' /> + 65 67481737
                </p>
                <p>
                  <i className='fas fa-print mr-3' /> + 65 67484812
                </p>
              </div>
            </div>
          </div>
        </section>
        <div
          className='text-center p-3'
          style={{ backgroundColor: "rgba(0, 0, 0, 0.2)" }}>
          &copy; 2015 Copyright:
          <a className='text-white' href='https://mdbootstrap.com/'>
            MDBootstrap.com
          </a>
        </div>
      </footer>
    </>
  );
};

export default Footer;
