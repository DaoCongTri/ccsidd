/** @format */

const contentPremiumConnect = [
  {
    id: 1,
    label: "How do we dial oversea?",
    content: (
      <p>
        To dial overseas in Singapore through CCS network, users should dial as
        follow:
        <br />
        1505 + Country Code + Area Code + Destination Number <br />
        For example, dialing a number in Los Angeles, US: <br />
        1505 (prefix) 1 (country code) 213 (area code) 5541212 (destination
        number) <br />
        For 15050 VQ Connect, user can dial as follow: <br />
        15050 (prefix) 1 (country code) 213 (area code) 5541212 (destination
        number)
      </p>
    ),
    controls: "premiumOne",
  },
  {
    id: 2,
    label:
      "What is the problem when you are unable to dial through CCS network?",
    content: (
      <p>
        If you are a new customer, tentatively your number had not been
        registered yet to our system. You will have to check with our customer
        service team to check if your number has been activated. If you are an
        existing customer, did your Company had recently done any changes to
        your phone system? If Yes, then we will need to send our technician to
        check on our customer premises devices (if any). <br /> If No, then you
        may need to contact our customer service hotline, preferable if you can
        provide us with the numbers dialed. We will do a thorough check and
        revert to you soonest. In most cases, it could be due to busy line at
        the destination receivable party or incorrect number dialed. There are
        some unique situation where the customer numbers had been barred by
        their fixed line provider to access 15XX prefixes. <br /> In this case,
        customer may need to contact their fixed line provider to release the
        block. Otherwise, please kindly contact our customer service hotline for
        verification. Our Customer Service will test and remedy the situation if
        failure of network is found.
      </p>
    ),
    controls: "premiumTwo",
  },
  {
    id: 3,
    label:
      "Do we need to install any Customer Premises Device (CPE) to utilize our services?",
    content: (
      <p>
        It will depend on the customers dialing habit.In fact, it is not
        mandatory to do so as customer can choose to dial CCS prefix 1505
        manually for making any international call.However, if customer would
        not want to change their usual way of dialing, like using already pre-
        programmed memory dial, we are able to install a device known as an
        autodialer. This CPE is able to perform the necessary digit conversion
        and route all of the customer calls to CCS voice network.In some phone
        system, programming can be done within itself by a certified phone
        system engineer, depending on the make of the phone system.
      </p>
    ),
    controls: "premiumThree",
  },
  {
    id: 4,
    label: "If we are using ISDN-30, are we able to use CCS services?",
    content: (
      <p>
        Yes, customer who are using ISDN-30 connected to their digital phone
        system are able to utlise CCS voice network by either manual dial,
        programming to be done on the phone system or by installing an
        autodialer that is meant for ISDN-30 lines. <br /> However, we do not
        support lines on ISDN 2B+D as we do not have a supported autodialer.
        Customers with such lines will only be possible to dial through us
        manually.
      </p>
    ),
    controls: "premiumFour",
  },
  {
    id: 5,
    label: "Why are we able to provide such services at such low cost?",
    content: (
      <p>
        The reason is that we are buying bulk wholesale minutes. <br /> We have
        worked with various carriers since we started our services, which helps
        us in maintaining consistent low rates that we can provide to our
        customers.
      </p>
    ),
    controls: "premiumFive",
  },
  {
    id: 6,
    label: "Why do phone numbers in other countries has a leading 0?",
    content: (
      <p>
        Singapore is a small country where we are a single state country. In
        other countries, usually they are made up of different states. For
        example, in China, different states number will have different area
        code. Shanghai 21, Beijing 10. To call a number from Shanghai to
        Beijing, the user will dial 0 10 12345678. For user to call from
        Shanghai to Beijing, the user will dial 0 21 12345678. <br /> This is a
        standard way to dial a domestic or inter-state call. In some situation,
        if you call within the same province, it will still work without dialing
        the leading 0.
        <br /> If you are in Singapore, and your business associates business
        card states the phone numbers with a leading 0, you will need to omit
        the leading 0 and dial as follow:
        <br /> 0 21 12345678 <i className='fa-solid fa-arrow-right'></i> 1505 86
        21 12345678.
      </p>
    ),
    controls: "premiumSix",
  },
  {
    id: 7,
    label:
      "Why the international prefix for other countries are different from Singapore?",
    content: (
      <p>
        This is because the telecommunication business in Singapore is fully
        liberalized. There are many different Long Distance Calls provider,
        operating using different prefixes.
        <br /> In Singapore, we usually make IDD calls using 3-digit prefixes
        like 001,002, 008, 018,019 or 021 provided by FBO.
        <br /> For all SBO, a 4 digit prefix Is used with a leading 15 follow by
        2 other numbers that is issued by IDA. <br /> For CCS, our prefix will
        be 1505.
      </p>
    ),
    controls: "premiumSever",
  },
  {
    id: 8,
    label: "Can we use CCS 1505 or 15050 services while overseas?",
    content: (
      <p>
        While you are overseas and you want to call to a Singapore number from
        the foreign destination, you will need to dial either with a leading +
        or 00.
        <br /> These are the international Direct Dial prefix for most countries
        with more than 1 state or province.
        <br /> Please kindly take note that the 1505 Premium Connect / 15050 VQ
        Connect services we are providing is only applicable in Singapore.
      </p>
    ),
    controls: "premiumEight",
  },
  {
    id: 9,
    label:
      "How can we make protection to prevent Internet Protocol (IP) enabled phone systems being hacked?",
    content: (
      <p>
        In order to fence off attacks from hackers, the most important thing to
        do is to ensure that the default administrator password to the phone
        system is changed. Possibly, it should be change at least at intervals
        of 3 months. If the IP feature is not utilised, the phone system should
        never leave to connect to the Internet. Also, customers should not allow
        any IP enabled voice device to be installed within any part of the phone
        system unless the installation is properly secured either by Company
        Administrator or its product specialists. <br /> It must be remember
        that service providers are only able to detect such attacks when there
        is a surge amount of calls leading to non-frequently called
        destinations.
        <br /> These destinations are usually very high in costs and somehow
        that even service provider are unable to notice due to the high volume
        of traffic passing through the routes they are using.
        <br /> Therefore, prevention is always the safest bet to ensure that
        your phone system is properly secured before enabling the IP
        capabilities. If the phone systems is compromised, then it may result to
        serious losses to the customer as in most of the situation, the customer
        may have to bear the costs for all the unsolicted calls.
      </p>
    ),
    controls: "premiumNine",
  },
];
const contentMobileService = [
  {
    id: 1,
    label: "How do we subscribe to CCS Mobile Services?",
    content: (
      <p>
        To know more about CCS Mobile Services, you can contact our Sales
        representatives at sales@ccsidd.com.
        <br /> We are able to offer attractive mobile packages as compared to
        mainstream service provider.
        <br /> We welcome corporate customers to hear our sales presentation and
        understand the posssibility to save on Company mobile bills.
      </p>
    ),
    controls: "mobileServiceOne",
  },
  {
    id: 2,
    label: "Who will be invoicing for utilizing our mobile services?",
    content:
      "Since we are licensed as MVNO, CCS is able to issue invoice directly to customers. All payments shall be paid directly to CCS.",
    controls: "mobileServiceTwo",
  },
  {
    id: 3,
    label:
      "Whenever there is any issue with the mobile lines, who should we contact?",
    content: (
      <p>
        For any mobile issues while subscribed with CCS, customers can contact
        our CCS customer service team via our hotline at +6567481737 or simply
        send an email to us via service@ccsidd.com Our operating hours will be
        from 09:00H to 18:00H from Monday to Friday. For any urgent cases after
        office hours or during weekends, customers can contact their respective
        Sales representative where they will be able to assist in resolving
        their issues.
        <br /> We will try our utmost to ensure good after sales service is
        maintained. However, we acknowledge there is always limitation even
        within the mainstream service providers in Singapore, and we may not be
        able to fulfill certain requests after office hours.
      </p>
    ),
    controls: "mobileServiceThree",
  },
];
const contentCallBack = {
  id: 1,
  label: "What is CCS Callback Services?",
  content: (
    <p>
      Callback is an old technology but effective way of saving costs to make a
      call while roaming. It will be able to cut down probably about 50% of the
      cost if users were to dial directly from their mobile phone while roaming.
      For CCS Callback Service, we will provide customer with a Singapore Number
      to dial to.
      <br /> This number is basically a triggering number. What it does is that
      when dialed, it will be able to reach our Singapore network, and our
      system will hang up the call upon receiving it. This is an non-connected
      call which therefore it’s not chargeable. Upon triggering, our network
      will do a call back to the subscriber’s mobile number as registered in
      advance with us.
      <br /> The subscriber will then answer the incoming call, and will be
      asked to dial the destination numbers and end with a # key. Customer
      should dial the country code, follow by area code, follow by destination
      and end with a # key. Once the # key is received, our system will dial to
      reach the number of the receiving party.
    </p>
  ),
  controls: "callBackOne",
};
const contentCallForward = {
  id: 1,
  label: "What is CCS Call Forwarding Services?",
  content: (
    <p>
      CCS Call Forwarding Services is to help customers to save on incoming
      calls or re-routed calls while roaming. It will work with customers
      holding with a local SIM card in the roaming country. When subscribed to
      CCS Call Forwarding services, subscribers will be given a Singapore local
      number. Customer will have to inform CCS of the number of the local SIM
      card of the roaming country. <br /> CCS network will map our Singapore
      local number to the local SIM card number of the roaming country. Just
      before departure from Singapore, subscriber should divert their Singapore
      Mobile number to the Singapore Local number we had assigned to them. In
      doing so, any incoming calls will be diverted to CCS network, where we
      will map to the subscriber’s overseas SIM card. The subscriber can then be
      able to receive any incoming calls when someone dials the Singapore Mobile
      number.
    </p>
  ),
  controls: "callForwardOne",
};
const contentRTONE = [
  {
    id: 1,
    label: "What is CCS R Tone Services?",
    content: (
      <p>
        CCS R Tone Services is the latest services we have introduced, in
        helping customers to save costs in making calls while they are roaming.
        <br />
        We have created a mobile app that will work on most popular mobile
        Operating Systems, to allow our subscribers to call via the app.
        <br /> It is a VOIP technology where calls are transmitted via WIFI or
        3G/4G network.
      </p>
    ),
    controls: "rtoneOne",
  },
  {
    id: 2,
    label: "Is it safe to call through R Tone?",
    content:
      "CCS has invested very much effort in making R Tone as secure as possible. We have been testing our apps with an in-house developed systems to ensure calls are safe and secured.",
    controls: "rtoneTwo",
  },
  {
    id: 3,
    label: "How is R Tone voice quality compares to our normal phone calls?",
    content: (
      <p>
        R Tone if under a good data network connectivity can achieve voice
        quality as good as what we are able to achieve as in our mobile call. We
        are spending good effort to improvise and ensure our voice can achieve
        its best quality even under low bandwidth connectivity.
        <br /> However, R Tone will not function if data network is not
        available. While this is so, subscribers can still fall back on the
        traditional means to make a phone call while roaming.
      </p>
    ),
    controls: "rtoneThree",
  },
  {
    id: 4,
    label:
      "Is there any difference between R Tone from apps like Instant Messaging (IM) applications?",
    content: (
      <p>
        Yes. There is some identical differences where R Tone allows user to
        call to any numbers in the world, where the rest are probably able to
        call to another mobile who are using the same app. While we are still
        improvising the R Tone Services, it will further develop to a better
        product than the mentioned apps.
      </p>
    ),
    controls: "rtoneFour",
  },
];
const contentDefinitions = [
  {
    id: 1,
    label: "International Direct Dial",
    content: (
      <p>
        IDD refers to a service offers by operators to make International Calls
        using a defined prefix.
      </p>
    ),
    controls: "definitionsOne",
  },
  {
    id: 2,
    label: "Service Based Operator",
    content: (
      <p>
        When the telecommunication market liberalised in the year 2001, The
        Infocomm Development Authority of Singapore defined the
        telecommunication licenses issued into 2 different categories, namely
        Service Based Operator (SBO) and Fixed Based Operator (FBO) licenses.
        For SBO, it is further defined into 2 sub categories, namely CLASS and
        INDIVIDUAL licenses. A full description on each licenses can be obtained
        via Infocomm Development Authority (IDA) As its name apply, licensee
        will be able to provide telecommunication services, with no oblgation to
        building infrastructures as required by Fixed Based Operator (FBO).
      </p>
    ),
    controls: "definitionsTwo",
  },
  {
    id: 3,
    label: "Fixed Based Operator",
    content: (
      <p>
        FBO refers to the first tier telecommunication operators in Singapore.
        For a full listing of FBO operators in Singapore, please kindly refers
        to{" "}
        <span className='text-primary'>
          Infocomm Development Authority (IDA)
        </span>
      </p>
    ),
    controls: "definitionsThree",
  },
  {
    id: 4,
    label: "Public Switch Telecommunication Network (PSTN)",
    content: (
      <p>
        The PSTN refers to the telecommunication network that is linked between
        all the local phone lines and mobile lines service provider within a
        state or a country. It is a fundamental infrastructure that is built to
        ensure good reliability of the telecommunication network, to link up the
        every household fixed lines and mobile users to facilitate communication
        via a robust voice network.
      </p>
    ),
    controls: "definitionsFour",
  },
  {
    id: 5,
    label: "Mobile Virtual Network Operator (MVNO)",
    content: (
      <p>
        MVNO refers to an operator providing mobile services to customers at
        their own branding, without owning and maintaining a physical mobile
        network. MVNO will have its liberty to work with any full fledged mobile
        operator to deliver their services.
      </p>
    ),
    controls: "definitionsFive",
  },
  {
    id: 6,
    label: "Tandem Based Network (TDM network)",
    content: (
      <p>
        A TDM network is a traditional voice network where voice quality are at
        its best possible. It is a wide band uncompressed network where voice
        traffic per channel is allocated with a full 64K bits of bandwidth.
      </p>
    ),
    controls: "definitionsSix",
  },
  {
    id: 7,
    label: "Customer Premises Equipment (CPE)",
    content: (
      <p>
        A CPE refers to devices or equipments that are installed in customer’s
        premises. It could be an Autodailer, Gateway, etc...
      </p>
    ),
    controls: "definitionsSever",
  },
  {
    id: 8,
    label: "Auto Dialer",
    content: (
      <p>
        Auto Dialer refers to a device which is installed in customer premises
        to perform simple prefix conversions, so that the user can maintain
        their usual dialing pattern. Basically, the device acts like part of the
        telephone wire, only when triggered upon when a match of the prefix that
        the user had dialed. For example, when user dials an international
        prefix 00X 1 213 554 1212, the auto dialer will be able to identify the
        prefix 00X, and replaced it with CCS prefix as 1505.
        <br /> Therefore when dialing out upon conversion, the number will then
        be dial as 1505 1 213 554 1212. To the user, the process is seamless.
        However, the Auto Dialer has to be powered on in order to be
        functional.If it is powered off, it is just like a wire to part of the
        telephone cable.
      </p>
    ),
    controls: "definitionsEight",
  },
  {
    id: 9,
    label: "Centrex System",
    content: (
      <p>
        Centrex System refers to a form of service where customer need not
        purchase a physical phone system to enjoy the full features of a phone
        system. This is usually provided by service provider where they will
        have to lay the phone lines to customer premises. Though it looks like a
        direct line, the system allows customers to group a number of phone
        lines within an office and perform call control, like call transfer,
        call re-direct which is featured in a standard phone system. There is no
        physical equipments needed to be installed within customer premises
        other than the phone lines.
      </p>
    ),
    controls: "definitionsNine",
  },
  {
    id: 10,
    label: "Instant Messaging (IM)",
    content: (
      <p>
        Instant messaging is a form of messaging application similar to the
        chatting application used in desktop version in the earlier years. The
        delivery of messages can now be very reliable under a good data
        connection network, where users can view if the contacts are online or
        offline. It can also determined if the messages is being received or
        read by the receivers. There are 2 forms of how IM stores the messages.
        All the messages can be stored within the phone itself or it can be
        stored in a server. For IM that stores messages in the phone, messages
        will be lost if the app is installed onto another phone unless a backup
        is done and restore in the new phone. For server kept messages, you can
        still recover the messages for a period of time even if you may have
        installed the app in a new phone. <br /> The duration of how long to
        keep the messages can be configurable as well. IM has been an uproaring
        trend to the traditional Short Message System (SMS) used in mobile
        phones network. It has more interesting way to present gesture with
        emotion icons where traditional SMS is dull and limited to the amount of
        text send per message. The interface for IM also allows transmission of
        files across the users, which can serve like a mini collaboration tools.
        However, there is limitation to the use of IM, both users must installed
        the same application in order to use the services. Also, it has always
        been questionable on how secure when sending messages via IM unless
        service provider put in place some sort of encryption for point to point
        messaging.
      </p>
    ),
    controls: "definitionsTen",
  },
];
const contentDowload = [
  {
    id: "1",
    label: "CS Mobile Terms and Conditions",
    dowloadForm:
      "https://ccsidd.com/download/CCS_Mobile_Terms_and_Conditions.pdf",
  },
  {
    id: "2",
    label: "CCS International Call Forwarding (ICF) Service",
    dowloadForm:
      "https://ccsidd.com/download/CCS_Letterhead_ICF_Notification_01112017.pdf",
  },
  {
    id: "3",
    label: "CCS Rateplan",
    dowloadForm:
      "https://ccsidd.com/download/CCS_Letterhead_ICF_Notification_01112017.pdf",
  },
  {
    id: "4",
    label: "CCS Service Agreement",
    dowloadForm: "https://ccsidd.com/download/CCS_Service_Agreement.pdf",
  },
  {
    id: "5",
    label: "Equipment Service Plan Undertaking",
    dowloadForm: "https://ccsidd.com/download/CCS_Service_Agreement.pdf",
  },
  {
    id: "6",
    label: "FAQ on R Tone",
    dowloadForm: "https://ccsidd.com/download/FAQ_on_R_Tone.pdf",
  },
  {
    id: "7",
    label: "R Tone additoinal agreement",
    dowloadForm: "https://ccsidd.com/download/R_Tone_additoinal_agreement.pdf",
  },
  {
    id: "8",
    label: "CCS IDD New Registration",
    dowloadForm: "https://ccsidd.com/download/CCS_IDD_New_Registration.pdf",
  },
  {
    id: "9",
    label: "CCS Mobile Registration Form",
    dowloadForm: "https://ccsidd.com/download/CCS_Mobile_Registration_Form.pdf",
  },
  {
    id: "10",
    label: "Data Passport 75 Countries Coverage",
    dowloadForm:
      "https://ccsidd.com/download/Data_Passport_75_Countries_Coverage.pdf",
  },
  {
    id: "11",
    label: "Transfer Fee Notice",
    dowloadForm: "https://ccsidd.com/download/Transfer_Fee_Notice.pdf",
  },
];
const ResourcesHomePage = () => {
  return (
    <>
      <section id='resources' className='row align-items-center'>
        <div className='col-12'>
          <div className='container'>
            <h1 className='title'>Resources</h1>
            <div
              className='row row-cols-auto g-0 nav nav-pills align-items-center justify-content-center'
              id='pills-tab'
              role='tablist'>
              <div className='col nav-item' role='presentation'>
                <button
                  className='nav-link active'
                  id='premium-connect-tab'
                  data-bs-toggle='pill'
                  data-bs-target='#premium-connect'
                  type='button'
                  role='tab'
                  aria-controls='premium-connect'
                  aria-selected='true'>
                  <span className='d-none d-md-block'>
                    1505 PREMIUM CONNECT <br /> 15050 VQ CONNECT
                  </span>
                  <i className='fa fa-plug d-block d-md-none'></i>
                </button>
              </div>
              <div className='col nav-item' role='presentation'>
                <button
                  className='nav-link'
                  id='mobile-service-tab'
                  data-bs-toggle='pill'
                  data-bs-target='#mobile-service'
                  type='button'
                  role='tab'
                  aria-controls='mobile-service'
                  aria-selected='false'>
                  <span className='d-none d-md-block'>MOBILE SERVICE</span>
                  <i className='fa fa-mobile d-block d-md-none'></i>
                </button>
              </div>
              <div className='col nav-item' role='presentation'>
                <button
                  className='nav-link'
                  id='c_b-service-tab'
                  data-bs-toggle='pill'
                  data-bs-target='#c_b-service'
                  type='button'
                  role='tab'
                  aria-controls='c_b-service'
                  aria-selected='false'>
                  <span className='d-none d-md-block'>CALL BACK SERVICE</span>
                  <i className='fa fa-phone d-block d-md-none'></i>
                </button>
              </div>
              <div className='col nav-item' role='presentation'>
                <button
                  className='nav-link'
                  id='c_f-service-tab'
                  data-bs-toggle='pill'
                  data-bs-target='#c_f-service'
                  type='button'
                  role='tab'
                  aria-controls='c_f-service'
                  aria-selected='false'>
                  <span className='d-none d-md-block'>
                    CALL FORWARDING SERVICE
                  </span>
                  <i className='fa fa-phone d-block d-md-none'></i>
                </button>
              </div>
              <div className='col nav-item' role='presentation'>
                <button
                  className='nav-link'
                  id='rtone-service-tab'
                  data-bs-toggle='pill'
                  data-bs-target='#rtone-service'
                  type='button'
                  role='tab'
                  aria-controls='rtone-service'
                  aria-selected='false'>
                  <span className='d-none d-md-block'>R TONE SERVICE</span>
                  <i className='fa fa-microphone d-block d-md-none'></i>
                </button>
              </div>
              <div className='col nav-item' role='presentation'>
                <button
                  className='nav-link'
                  id='definitions-tab'
                  data-bs-toggle='pill'
                  data-bs-target='#definitions'
                  type='button'
                  role='tab'
                  aria-controls='definitions'
                  aria-selected='false'>
                  <span className='d-none d-md-block'>DEFINITIONS</span>
                  <i className='fa fa-info d-block d-md-none'></i>
                </button>
              </div>
              <div className='col nav-item' role='presentation'>
                <button
                  className='nav-link'
                  id='dowload-tab'
                  data-bs-toggle='pill'
                  data-bs-target='#dowload'
                  type='button'
                  role='tab'
                  aria-controls='dowload'
                  aria-selected='false'>
                  <span className='d-none d-md-block'>DOWLOAD</span>
                  <i className='fa-solid fa-download d-block d-md-none'></i>
                </button>
              </div>
            </div>
            <div className='tab-content my-5' id='pills-tabContent'>
              <div
                className='tab-pane fade show active'
                id='premium-connect'
                role='tabpanel'
                aria-labelledby='premium-connect-tab'
                tabIndex={0}>
                <div className='accordion' id='accordionExample'>
                  {contentPremiumConnect.map((item, index) => {
                    return (
                      <div className='accordion-item' key={index}>
                        <h2 className='accordion-header'>
                          <button
                            className='accordion-button'
                            type='button'
                            data-bs-toggle='collapse'
                            data-bs-target={"#" + item.controls}
                            aria-expanded='false'
                            aria-controls={item.controls}>
                            {item.label}
                          </button>
                        </h2>
                        <div
                          id={item.controls}
                          className='accordion-collapse collapse'
                          data-bs-parent='#accordionExample'>
                          <div className='accordion-body'>{item.content}</div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
              <div
                className='tab-pane fade'
                id='mobile-service'
                role='tabpanel'
                aria-labelledby='mobile-service-tab'
                tabIndex={0}>
                <div className='accordion' id='accordionExample'>
                  {contentMobileService.map((item, index) => {
                    return (
                      <div className='accordion-item' key={index}>
                        <h2 className='accordion-header'>
                          <button
                            className='accordion-button'
                            type='button'
                            data-bs-toggle='collapse'
                            data-bs-target={"#" + item.controls}
                            aria-expanded='false'
                            aria-controls={item.controls}>
                            {item.label}
                          </button>
                        </h2>
                        <div
                          id={item.controls}
                          className='accordion-collapse collapse'
                          data-bs-parent='#accordionExample'>
                          <div className='accordion-body'>{item.content}</div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
              <div
                className='tab-pane fade'
                id='c_b-service'
                role='tabpanel'
                aria-labelledby='c_b-service-tab'
                tabIndex={0}>
                <div className='accordion' id='accordionExample'>
                  <div className='accordion-item' key={contentCallBack.id}>
                    <h2 className='accordion-header'>
                      <button
                        className='accordion-button'
                        type='button'
                        data-bs-toggle='collapse'
                        data-bs-target={"#" + contentCallBack.controls}
                        aria-expanded='false'
                        aria-controls={contentCallBack.controls}>
                        {contentCallBack.label}
                      </button>
                    </h2>
                    <div
                      id={contentCallBack.controls}
                      className='accordion-collapse collapse'
                      data-bs-parent='#accordionExample'>
                      <div className='accordion-body'>
                        {contentCallBack.content}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className='tab-pane fade'
                id='c_f-service'
                role='tabpanel'
                aria-labelledby='c_f-service-tab'
                tabIndex={0}>
                <div className='accordion' id='accordionExample'>
                  <div className='accordion-item' key={contentCallForward.id}>
                    <h2 className='accordion-header'>
                      <button
                        className='accordion-button'
                        type='button'
                        data-bs-toggle='collapse'
                        data-bs-target={"#" + contentCallForward.controls}
                        aria-expanded='false'
                        aria-controls={contentCallForward.controls}>
                        {contentCallForward.label}
                      </button>
                    </h2>
                    <div
                      id={contentCallForward.controls}
                      className='accordion-collapse collapse'
                      data-bs-parent='#accordionExample'>
                      <div className='accordion-body'>
                        {contentCallForward.content}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className='tab-pane fade'
                id='rtone-service'
                role='tabpanel'
                aria-labelledby='rtone-service-tab'
                tabIndex={0}>
                <div className='accordion' id='accordionExample'>
                  {contentRTONE.map((item, index) => {
                    return (
                      <div className='accordion-item' key={index}>
                        <h2 className='accordion-header'>
                          <button
                            className='accordion-button'
                            type='button'
                            data-bs-toggle='collapse'
                            data-bs-target={"#" + item.controls}
                            aria-expanded='false'
                            aria-controls={item.controls}>
                            {item.label}
                          </button>
                        </h2>
                        <div
                          id={item.controls}
                          className='accordion-collapse collapse'
                          data-bs-parent='#accordionExample'>
                          <div className='accordion-body'>{item.content}</div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
              <div
                className='tab-pane fade'
                id='definitions'
                role='tabpanel'
                aria-labelledby='definitions-tab'
                tabIndex={0}>
                <div className='accordion' id='accordionExample'>
                  {contentDefinitions.map((item, index) => {
                    return (
                      <div className='accordion-item' key={index}>
                        <h2 className='accordion-header'>
                          <button
                            className='accordion-button'
                            type='button'
                            data-bs-toggle='collapse'
                            data-bs-target={"#" + item.controls}
                            aria-expanded='false'
                            aria-controls={item.controls}>
                            {item.label}
                          </button>
                        </h2>
                        <div
                          id={item.controls}
                          className='accordion-collapse collapse'
                          data-bs-parent='#accordionExample'>
                          <div className='accordion-body'>{item.content}</div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
              <div
                className='tab-pane fade'
                id='dowload'
                role='tabpanel'
                aria-labelledby='dowload-tab'
                tabIndex={0}>
                <div className='row row-cols-1 row-cols-md-2 gy-5 align-items-center'>
                  {contentDowload.map((items) => {
                    return (
                      <div className='col' key={items.id}>
                        <img
                          src='/images/download-item.png'
                          width={50}
                          height={50}
                          alt=''
                        />
                        <a href='#' className='ms-3'>
                          {items.label}
                        </a>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ResourcesHomePage;
