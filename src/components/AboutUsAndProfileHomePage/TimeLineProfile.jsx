/** @format */

import { Carousel, Collapse, Timeline } from "antd";
const items1TimeLine = [
  {
    label: <p>1998</p>,
    children: (
      <p>
        Incorporated May 1998 in Singapore <br />
        Provided Callback Service using in-house <br />
        Developed traditional Callback
      </p>
    ),
    dot: (
      <img src='/images/timelineProfile/icons8-client_company@1x.png' alt='' />
    ),
  },
  {
    label: <p>1999</p>,
    children: (
      <p>
        Designed and developed DBDZ <br /> CallBack Switch using Immix
      </p>
    ),
    dot: <img src='/images/timelineProfile/icons8-router@1x.png' alt='' />,
  },
  {
    label: <p>2001</p>,
    children: (
      <p>
        Awarded Service Base <br />
        Operator (SBO) Class License from Infocomm <br />
        Development Authority of Singapore
      </p>
    ),
    dot: <img src='/images/timelineProfile/icons8-prize@1x.png' alt='' />,
  },
  {
    label: <p>2002</p>,
    children: (
      <p>
        Constructed VPN between
        <br /> Singapore to Hong Kong and Taiwan
      </p>
    ),
    dot: <img src='/images/timelineProfile/icons8-globe@1x.png' alt='' />,
  },
  {
    label: <p>2003</p>,
    children: (
      <p>
        Awarded Service Base <br /> Operator (SBO) Individual License from
        Infocomm <br /> Development Authority (IDA) of Singapore Constructed{" "}
        <br />
        Managed VoIP Network between Singapore and Korea Obtained <br /> 1505
        CCS IDD Access Code from IDA
      </p>
    ),
    dot: <img src='/images/timelineProfile/icons8-diploma_2@1x.png' alt='' />,
  },
];
const items2TimeLine = [
  {
    label: <p>2004</p>,
    children: <p>CCS 1505 IDD Service launch</p>,
    dot: <img src='/images/timelineProfile/icons8-rocket@1x.png' alt='' />,
  },
  {
    label: <p>2005</p>,
    children: <p>CS International Call Forwarding (ICF) Service Launch</p>,
    dot: <img src='/images/timelineProfile/icons8-rocket@1x.png' alt='' />,
  },
  {
    label: <p>2006</p>,
    children: (
      <p>
        CCS CallBack Service Launch <br /> CCS 15050 Budget IDD Service Launch
      </p>
    ),
    dot: <img src='/images/timelineProfile/icons8-rocket@1x.png' alt='' />,
  },
  {
    label: <p>2008</p>,
    children: (
      <p>
        CS Mobile Dialer Launch <br /> CCS Mobile & CB-Roaming Services Launch
        Obtain <br /> (MVNO) Mobile Virtual Network Operator license from <br />{" "}
        IDA Reselling Mobile services Incorporated subsidiary <br /> office at
        Batam, Indonesia
      </p>
    ),
    dot: <img src='/images/timelineProfile/icons8-root_server@1x.png' alt='' />,
  },
  {
    label: <p>2009</p>,
    children: (
      <p>
        Launch R-connect service apps <br /> Launch CCS Long Distance Calls apps
      </p>
    ),
    dot: (
      <img
        src='/images/timelineProfile/icons8-smartphone_tablet@1x.png'
        alt=''
      />
    ),
  },
];
const items3TimeLine = [
  {
    label: <p>2010</p>,
    children: <p>Acquired Digiphonic Systems Pte Ltd.</p>,
    dot: <img src='/images/timelineProfile/icons8-approval@1x.png' alt='' />,
  },
  {
    label: <p>2014</p>,
    children: (
      <p>Incorporated subsidiary office at Ho Chi Minh City, Vietnam</p>
    ),
    dot: <img src='/images/timelineProfile/ic-vnflag@1x.png' alt='' />,
  },
  {
    label: <p>2015</p>,
    children: (
      <p>
        CCS Dial 1505 apps (replace CCS Long Distance Calls apps) <br /> CCS R
        Tone Lite apps (replace R-connect apps)
      </p>
    ),
    dot: <img src='/images/timelineProfile/icons8-sms@1x.png' alt='' />,
  },
  {
    label: <p>2016</p>,
    children: <p>Abtain Digit 3 numbers (L3VL) form IDA R Tone apps launch</p>,
    dot: <img src='/images/timelineProfile/icons8-sent@1x.png' alt='' />,
  },
];
const items1Tab = [
  {
    key: "1",
    label: <p>1998</p>,
    children: (
      <p>
        Incorporated May 1998 in Singapore <br />
        Provided Callback Service using in-house <br />
        Developed traditional Callback
      </p>
    ),
    showArrow: false,
    extra: (
      <img src='/images/timelineProfile/icons8-client_company@1x.png' alt='' />
    ),
  },
  {
    key: "2",
    label: <p>1999</p>,
    children: (
      <p>
        Designed and developed DBDZ <br /> CallBack Switch using Immix
      </p>
    ),
    showArrow: false,
    extra: <img src='/images/timelineProfile/icons8-router@1x.png' alt='' />,
  },
  {
    key: "3",
    label: <p>2001</p>,
    children: (
      <p>
        Awarded Service Base <br />
        Operator (SBO) Class License from Infocomm <br />
        Development Authority of Singapore
      </p>
    ),
    showArrow: false,
    extra: <img src='/images/timelineProfile/icons8-prize@1x.png' alt='' />,
  },
  {
    key: "4",
    label: <p>2002</p>,
    children: (
      <p>
        Constructed VPN between
        <br /> Singapore to Hong Kong and Taiwan
      </p>
    ),
    showArrow: false,
    extra: <img src='/images/timelineProfile/icons8-globe@1x.png' alt='' />,
  },
  {
    key: "5",
    label: <p>2003</p>,
    children: (
      <p>
        Awarded Service Base <br /> Operator (SBO) Individual License from
        Infocomm <br /> Development Authority (IDA) of Singapore Constructed{" "}
        <br />
        Managed VoIP Network between Singapore and Korea Obtained <br /> 1505
        CCS IDD Access Code from IDA
      </p>
    ),
    showArrow: false,
    extra: <img src='/images/timelineProfile/icons8-diploma_2@1x.png' alt='' />,
  },
];
const items2Tab = [
  {
    key: "1",
    label: <p>2004</p>,
    children: <p>CCS 1505 IDD Service launch</p>,
    showArrow: false,
    extra: <img src='/images/timelineProfile/icons8-rocket@1x.png' alt='' />,
  },
  {
    key: "2",
    label: <p>2005</p>,
    children: <p>CS International Call Forwarding (ICF) Service Launch</p>,
    showArrow: false,
    extra: <img src='/images/timelineProfile/icons8-rocket@1x.png' alt='' />,
  },
  {
    key: "3",
    label: <p>2006</p>,
    children: (
      <p>
        CCS CallBack Service Launch <br /> CCS 15050 Budget IDD Service Launch
      </p>
    ),
    showArrow: false,
    extra: <img src='/images/timelineProfile/icons8-rocket@1x.png' alt='' />,
  },
  {
    key: "4",
    label: <p>2008</p>,
    children: (
      <p>
        CS Mobile Dialer Launch <br /> CCS Mobile & CB-Roaming Services Launch
        Obtain <br /> (MVNO) Mobile Virtual Network Operator license from <br />{" "}
        IDA Reselling Mobile services Incorporated subsidiary <br /> office at
        Batam, Indonesia
      </p>
    ),
    showArrow: false,
    extra: (
      <img src='/images/timelineProfile/icons8-root_server@1x.png' alt='' />
    ),
  },
  {
    key: "5",
    label: <p>2009</p>,
    children: (
      <p>
        Launch R-connect service apps <br /> Launch CCS Long Distance Calls apps
      </p>
    ),
    showArrow: false,
    extra: (
      <img
        src='/images/timelineProfile/icons8-smartphone_tablet@1x.png'
        alt=''
      />
    ),
  },
];
const items3Tab = [
  {
    key: "1",
    label: <p>2010</p>,
    children: <p>Acquired Digiphonic Systems Pte Ltd.</p>,
    showArrow: false,
    extra: <img src='/images/timelineProfile/icons8-approval@1x.png' alt='' />,
  },
  {
    key: "2",
    label: <p>2014</p>,
    children: (
      <p>Incorporated subsidiary office at Ho Chi Minh City, Vietnam</p>
    ),
    showArrow: false,
    extra: <img src='/images/timelineProfile/ic-vnflag@1x.png' alt='' />,
  },
  {
    key: "3",
    label: <p>2015</p>,
    children: (
      <p>
        CCS Dial 1505 apps (replace CCS Long Distance Calls apps) <br /> CCS R
        Tone Lite apps (replace R-connect apps)
      </p>
    ),
    showArrow: false,
    extra: <img src='/images/timelineProfile/icons8-sms@1x.png' alt='' />,
  },
  {
    key: "4",
    label: <p>2016</p>,
    children: <p>Abtain Digit 3 numbers (L3VL) form IDA R Tone apps launch</p>,
    showArrow: false,
    extra: <img src='/images/timelineProfile/icons8-sent@1x.png' alt='' />,
  },
];
const TimeLineProfile = () => {
  return (
    <>
      <div id='timeline'>
        <section className='d-none d-lg-block'>
          <Carousel
            dotPosition={"right"}
            dots={<div id='dotsCarousel'></div>}
            draggable
            // autoplay
            autoplaySpeed={4000}>
            <div id='contentTimeline'>
              <Timeline mode='alternate' items={items1TimeLine} />
            </div>
            <div id='contentTimeline'>
              <Timeline mode='alternate' items={items2TimeLine} />
            </div>
            <div id='contentTimeline'>
              <Timeline mode='alternate' items={items3TimeLine} />
            </div>
          </Carousel>
        </section>
        <section className='container contentCollapse d-block'>
          <Carousel
            dotPosition='right'
            dots={false}
            draggable
            // autoplay
            effect='scrollx'
            autoplaySpeed={3000}>
            <div>
              <Collapse items={items1Tab} />
            </div>
            <div>
              <Collapse items={items2Tab} />
            </div>
            <div>
              <Collapse items={items3Tab} />
            </div>
          </Carousel>
        </section>
      </div>
    </>
  );
};

export default TimeLineProfile;
