/** @format */

import TimeLineProfile from "./TimeLineProfile";
import { Typography } from "antd";
const { Paragraph } = Typography;
const AboutUsAndProfileHomePage = () => {
  return (
    <>
      <section id='aboutus' className='row align-items-center'>
        <div className='col-12'>
          <div className='container'>
            <h1 className='title'>About CCS</h1>
            <div className='row row-cols-1 row-cols-lg-2'>
              <div className='col' id='about-content'>
                <div className='d-none d-md-block' id='about-content-md'>
                  <p>
                    We, at first, would like to sincerely thank you for your
                    expression of interest.
                  </p>
                  <p>
                    Connecting Communication & Solutions Private Limited (CCS)
                    is service-based operator that specialized in providing
                    overseas call service (voice, fax, and inmarsat) to
                    corporate markets.
                  </p>
                  <p>
                    By taking advantages of the growth and acceptance of the
                    many different types of protocol and signaling as a low cost
                    and efficient communication medium, CCS has developed new
                    technologies and applications to exploit this potential to
                    created the perfect value added business.
                  </p>
                  <p>
                    CCS will always put our partners and customers at the
                    highest priority.
                  </p>
                  <p>
                    CCS will constantly strive to improve our services and
                    products to our customers.
                  </p>
                  <p>
                    We believe in promptness, excellent services and good
                    product support.
                  </p>
                  <p>
                    We will aim to constantly bring in innovative products and
                    services at a very competitive price to the marketplace so
                    that our customers and partners can enjoy the benefits of
                    the latest technology.
                  </p>
                  <p>
                    At CCS, one of our crucial main objectives is to provide
                    company with an alternation to re-route their IDD/Roaming
                    Calls at lower rates without compromising the voice quality.
                  </p>
                </div>
                <Paragraph
                  className='d-block d-md-none'
                  id='about-content-sm1'
                  ellipsis={{
                    rows: 10,
                    expandable: true,
                    symbol: "View more",
                  }}>
                  We, at first, would like to sincerely thank you for your
                  expression of interest. <br />
                  Connecting Communication & Solutions Private Limited (CCS) is
                  service-based operator that specialized in providing overseas
                  call service (voice, fax, and inmarsat) to corporate markets.{" "}
                  <br />
                  By taking advantages of the growth and acceptance of the many
                  different types of protocol and signaling as a low cost and
                  efficient communication medium, CCS has developed new
                  technologies and applications to exploit this potential to
                  created the perfect value added business. <br />
                  CCS will always put our partners and customers at the highest
                  priority.
                  <br />
                  CCS will constantly strive to improve our services and
                  products to our customers. <br />
                  We believe in promptness, excellent services and good product
                  support. <br />
                  We will aim to constantly bring in innovative products and
                  services at a very competitive price to the marketplace so
                  that our customers and partners can enjoy the benefits of the
                  latest technology. <br />
                  At CCS, one of our crucial main objectives is to provide
                  company with an alternation to re-route their IDD/Roaming
                  Calls at lower rates without compromising the voice quality.
                </Paragraph>
                <Paragraph
                  className='d-none'
                  id='about-content-sm2'
                  ellipsis={{
                    rows: 10,
                    expandable: true,
                    symbol: "View more",
                  }}>
                  We, at first, would like to sincerely thank you for your
                  expression of interest. <br />
                  Connecting Communication & Solutions Private Limited (CCS) is
                  service-based operator that specialized in providing overseas
                  call service (voice, fax, and inmarsat) to corporate markets.{" "}
                  <br />
                  By taking advantages of the growth and acceptance of the many
                  different types of protocol and signaling as a low cost and
                  efficient communication medium, CCS has developed new
                  technologies and applications to exploit this potential to
                  created the perfect value added business. <br />
                  CCS will always put our partners and customers at the highest
                  priority.
                  <br />
                  CCS will constantly strive to improve our services and
                  products to our customers. <br />
                  We believe in promptness, excellent services and good product
                  support. <br />
                  We will aim to constantly bring in innovative products and
                  services at a very competitive price to the marketplace so
                  that our customers and partners can enjoy the benefits of the
                  latest technology. <br />
                  At CCS, one of our crucial main objectives is to provide
                  company with an alternation to re-route their IDD/Roaming
                  Calls at lower rates without compromising the voice quality.
                </Paragraph>
              </div>
              <div className='col d-none d-md-block' id='about-image'>
                <img
                  src='/images/splash/about.png'
                  width='100%'
                  height='100%'
                  alt=''
                />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id='profile' className='row align-items-center'>
        <div className='col-12'>
          <div className='container'>
            <h1 className='title'>Profile</h1>
          </div>
          <TimeLineProfile />
        </div>
      </section>
    </>
  );
};

export default AboutUsAndProfileHomePage;
